package ru.t1consulting.nkolesnik.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1consulting.nkolesnik.tm.api.service.*;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.ModelNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.*;
import ru.t1consulting.nkolesnik.tm.marker.DataCategory;
import ru.t1consulting.nkolesnik.tm.model.Project;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.model.User;
import ru.t1consulting.nkolesnik.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import java.util.UUID;
import java.util.Vector;

@Category(DataCategory.class)
public class UserServiceTest {

    @NotNull
    private static final String USER_LOGIN_PREFIX = "TEST_USER_LOGIN";

    @NotNull
    private static final String USER_EMAIL_PREFIX = "TEST_USER_@EMAIL";

    @NotNull
    private static final String USER_PASSWORD_PREFIX = "TEST_USER_PASSWORD";

    @NotNull
    private static final String USER_PASSWORD_SECRET = "123654789";

    @NotNull
    private static final Integer USER_PASSWORD_ITERATION = 3;

    @NotNull
    private static final String USER_FIRST_NAME = "TEST";

    @NotNull
    private static final String USER_MIDDLE_NAME = "TEST";

    @NotNull
    private static final String USER_LAST_NAME = "TEST";

    private static final long REPOSITORY_SIZE = 100L;

    private static final long EMPTY_REPOSITORY_SIZE = 0L;

    @Nullable
    private static final User NULL_USER = null;

    @Nullable
    private static final String NULL_USER_ID = null;

    @Nullable
    private static final String EMPTY_USER_ID = null;

    @Nullable
    private static final String NULL_USER_EMAIL = null;

    @Nullable
    private static final String NULL_USER_LOGIN = null;

    @Nullable
    private static final Role NULL_USER_ROLE = null;

    @Nullable
    private static final String NULL_USER_PASSWORD = null;

    @Nullable
    private static final String EMPTY_USER_LOGIN = "";

    @Nullable
    private static final String EMPTY_USER_PASSWORD = "";

    @Nullable
    private static final String EMPTY_USER_EMAIL = "";

    @NotNull
    private static final String TASK_NAME_PREFIX = "TEST_TASK_NAME";

    @NotNull
    private static final String TASK_DESCRIPTION_PREFIX = "TEST_TASK_DESCRIPTION";

    @NotNull
    private static final String PROJECT_NAME_PREFIX = "PROJECT_TASK_NAME";

    @NotNull
    private static final String PROJECT_DESCRIPTION_PREFIX = "PROJECT_TASK_DESCRIPTION";

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String projectId = UUID.randomUUID().toString();

    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    private User user;

    @NotNull
    private List<Task> tasks;

    @NotNull
    private Project project;

    @BeforeClass
    public static void prepareTestEnvironment() {
        createDbBackup();
    }

    @AfterClass
    public static void restoreProdEnvironment() {
        restoreDbBackup();
    }

    @SneakyThrows
    private static void createDbBackup() {
        final IPropertyService propertyService = new PropertyService();
        final IConnectionService connectionService = new ConnectionService(propertyService);
        Connection connection = connectionService.getConnection();
        String sqlSaveUserData = "SELECT * INTO \"backup_users\" FROM \"users\";";
        String sqlSaveProjectData = "SELECT * INTO \"backup_projects\" FROM \"projects\";";
        String sqlSavTaskeData = "SELECT * INTO \"backup_tasks\" FROM \"tasks\";";
        String sqlClearUserTable = "DELETE FROM \"users\";";
        String sqlClearProjectTable = "DELETE FROM \"projects\";";
        String sqlClearTaskTable = "DELETE FROM \"tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlSaveUserData);
            statement.executeUpdate(sqlSaveProjectData);
            statement.executeUpdate(sqlSavTaskeData);
            statement.executeUpdate(sqlClearUserTable);
            statement.executeUpdate(sqlClearProjectTable);
            statement.executeUpdate(sqlClearTaskTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    private static void restoreDbBackup() {
        final IPropertyService propertyService = new PropertyService();
        final IConnectionService connectionService = new ConnectionService(propertyService);
        Connection connection = connectionService.getConnection();
        String sqlClearUserTable = "DELETE FROM \"users\";";
        String sqlClearProjectTable = "DELETE FROM \"projects\";";
        String sqlClearTaskTable = "DELETE FROM \"tasks\";";
        String sqlLoadUserData = "INSERT INTO \"users\" (SELECT * FROM \"backup_users\");";
        String sqlLoadProjectData = "INSERT INTO \"projects\" (SELECT * FROM \"backup_projects\");";
        String sqlLoadTaskData = "INSERT INTO \"tasks\" (SELECT * FROM \"backup_tasks\");";
        String sqlDropBackupUserTable = "DROP TABLE \"backup_users\";";
        String sqlDropBackupProjectTable = "DROP TABLE \"backup_projects\";";
        String sqlDropBackupTaskTable = "DROP TABLE \"backup_tasks\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlClearUserTable);
            statement.executeUpdate(sqlClearProjectTable);
            statement.executeUpdate(sqlClearTaskTable);
            statement.executeUpdate(sqlLoadUserData);
            statement.executeUpdate(sqlLoadProjectData);
            statement.executeUpdate(sqlLoadTaskData);
            statement.executeUpdate(sqlDropBackupUserTable);
            statement.executeUpdate(sqlDropBackupProjectTable);
            statement.executeUpdate(sqlDropBackupTaskTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Before
    public void setup() {
        user = createUser();
        tasks = createManyTasks();
        project = createOneProject();
    }

    @After
    public void cleanup() {
        taskService.clear();
        projectService.clear();
        userService.clear();
    }

    @Test
    public void create() {
        userService.add(user);
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> userService.create(EMPTY_USER_LOGIN, USER_PASSWORD_PREFIX)
        );
        Assert.assertThrows(
                LoginEmptyException.class,
                () -> userService.create(NULL_USER_LOGIN, USER_PASSWORD_PREFIX)
        );
        Assert.assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(USER_LOGIN_PREFIX + "TEST", EMPTY_USER_PASSWORD)
        );
        Assert.assertThrows(
                PasswordEmptyException.class,
                () -> userService.create(USER_LOGIN_PREFIX + "TEST", NULL_USER_PASSWORD)
        );
        Assert.assertThrows(
                LoginAlreadyExistException.class,
                () -> userService.create(USER_LOGIN_PREFIX, USER_PASSWORD_PREFIX)
        );
        Assert.assertThrows(
                LoginAlreadyExistException.class,
                () -> userService.create(USER_LOGIN_PREFIX, USER_PASSWORD_PREFIX, USER_EMAIL_PREFIX + "TEST")
        );
        Assert.assertThrows(
                RoleIsEmptyException.class,
                () -> userService.create(
                        USER_LOGIN_PREFIX + "TEST",
                        USER_PASSWORD_PREFIX,
                        EMPTY_USER_EMAIL + "TEST",
                        NULL_USER_ROLE
                )
        );
        Assert.assertThrows(
                EmailEmptyException.class,
                () -> userService.create(USER_LOGIN_PREFIX + "TEST", USER_PASSWORD_PREFIX, EMPTY_USER_EMAIL)
        );
        Assert.assertThrows(
                EmailEmptyException.class,
                () -> userService.create(USER_LOGIN_PREFIX + "TEST", USER_PASSWORD_PREFIX, NULL_USER_EMAIL)
        );
        Assert.assertThrows(
                EmailAlreadyExistException.class,
                () -> userService.create(USER_LOGIN_PREFIX + "TEST", USER_PASSWORD_PREFIX, USER_EMAIL_PREFIX)
        );
        userService.create(USER_LOGIN_PREFIX + "TEST", USER_PASSWORD_PREFIX + "TEST");
        userService.create(USER_LOGIN_PREFIX + "TEST1", USER_PASSWORD_PREFIX + "TEST", Role.USUAL);
        userService.create(
                USER_LOGIN_PREFIX + "TEST2",
                USER_PASSWORD_PREFIX + "TEST",
                USER_EMAIL_PREFIX + "TEST2"
        );
        userService.create(
                USER_LOGIN_PREFIX + "TEST3",
                USER_PASSWORD_PREFIX + "TEST",
                USER_EMAIL_PREFIX + "TEST3",
                Role.USUAL
        );
        Assert.assertEquals(5, userService.getSize());
    }

    @Test
    public void findByLogin() {
        userService.add(user);
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(EMPTY_USER_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(NULL_USER_LOGIN));
        Assert.assertNull(userService.findByLogin(UUID.randomUUID().toString()));
        @Nullable final User repositoryUser = userService.findByLogin(USER_LOGIN_PREFIX);
        Assert.assertNotNull(repositoryUser);
        Assert.assertEquals(user.getId(), repositoryUser.getId());
    }

    @Test
    public void findByEmail() {
        userService.add(user);
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(EMPTY_USER_EMAIL));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(NULL_USER_EMAIL));
        Assert.assertNull(userService.findByEmail(UUID.randomUUID().toString()));
        @Nullable final User repositoryUser = userService.findByEmail(USER_EMAIL_PREFIX);
        Assert.assertNotNull(repositoryUser);
        Assert.assertEquals(user.getId(), repositoryUser.getId());
    }

    @Test
    public void setPassword() {
        userService.add(user);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> userService.setPassword(NULL_USER_ID, USER_PASSWORD_PREFIX)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> userService.setPassword(EMPTY_USER_ID, USER_PASSWORD_PREFIX)
        );
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(userId, EMPTY_USER_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(userId, NULL_USER_PASSWORD));
        @Nullable final String oldPassword = user.getPasswordHash();
        userService.setPassword(userId, "NEW PASSWORD");
        @Nullable User repositoryUser = userService.findById(user.getId());
        Assert.assertNotNull(repositoryUser);
        Assert.assertNotEquals(oldPassword, repositoryUser.getPasswordHash());
    }

    @Test
    public void updateUser() {
        userService.add(user);
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> userService.updateUser(NULL_USER_ID, USER_FIRST_NAME, USER_MIDDLE_NAME, USER_LAST_NAME)
        );
        Assert.assertThrows(
                UserIdEmptyException.class,
                () -> userService.updateUser(EMPTY_USER_ID, USER_FIRST_NAME, USER_MIDDLE_NAME, USER_LAST_NAME)
        );
        userService.updateUser(userId, USER_FIRST_NAME, USER_MIDDLE_NAME, USER_LAST_NAME);
        @Nullable User repositoryUser = userService.findById(user.getId());
        Assert.assertNotNull(repositoryUser);
        Assert.assertEquals(repositoryUser.getFirstName(), USER_FIRST_NAME);
        Assert.assertEquals(repositoryUser.getMiddleName(), USER_MIDDLE_NAME);
        Assert.assertEquals(repositoryUser.getLastName(), USER_LAST_NAME);
    }

    @Test
    public void remove() {
        taskService.add(tasks);
        projectService.add(project);
        userService.add(user);
        Assert.assertThrows(ModelNotFoundException.class, () -> userService.remove(NULL_USER));
        Assert.assertEquals(REPOSITORY_SIZE, taskService.getSize(userId));
        Assert.assertEquals(1, projectService.getSize(userId));
        Assert.assertEquals(1, userService.getSize());
        userService.remove(user);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, taskService.getSize(userId));
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, projectService.getSize(userId));
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, userService.getSize());
    }

    @Test
    public void removeByLogin() {
        userService.add(user);
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(EMPTY_USER_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(NULL_USER_LOGIN));
        Assert.assertThrows(UserNotFoundException.class, () -> userService.removeByLogin(UUID.randomUUID().toString()));
        Assert.assertEquals(user.getId(), userService.removeByLogin(USER_LOGIN_PREFIX).getId());
    }

    @Test
    public void isLoginExist() {
        userService.add(user);
        Assert.assertFalse(userService.isLoginExist(EMPTY_USER_LOGIN));
        Assert.assertFalse(userService.isLoginExist(NULL_USER_LOGIN));
        Assert.assertFalse(userService.isLoginExist(UUID.randomUUID().toString()));
        Assert.assertTrue(userService.isLoginExist(USER_LOGIN_PREFIX));
    }

    @Test
    public void isEmailExist() {
        userService.add(user);
        Assert.assertFalse(userService.isEmailExist(EMPTY_USER_EMAIL));
        Assert.assertFalse(userService.isEmailExist(NULL_USER_EMAIL));
        Assert.assertFalse(userService.isEmailExist(UUID.randomUUID().toString()));
        Assert.assertTrue(userService.isEmailExist(USER_EMAIL_PREFIX));
    }

    @Test
    public void lockUserByLogin() {
        userService.add(user);
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(EMPTY_USER_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(NULL_USER_LOGIN));
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> userService.lockUserByLogin(UUID.randomUUID().toString())
        );
        userService.lockUserByLogin(USER_LOGIN_PREFIX);
        @Nullable final User repositoryUser = userService.findById(user.getId());
        Assert.assertNotNull(repositoryUser);
        Assert.assertTrue(repositoryUser.getLocked());
    }

    @Test
    public void unlockUserByLogin() {
        userService.add(user);
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(EMPTY_USER_LOGIN));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unlockUserByLogin(NULL_USER_LOGIN));
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> userService.unlockUserByLogin(UUID.randomUUID().toString())
        );
        userService.lockUserByLogin(USER_LOGIN_PREFIX);
        @Nullable User repositoryUser = userService.findById(user.getId());
        Assert.assertNotNull(repositoryUser);
        Assert.assertTrue(repositoryUser.getLocked());
        userService.unlockUserByLogin(USER_LOGIN_PREFIX);
        repositoryUser = userService.findById(user.getId());
        Assert.assertNotNull(repositoryUser);
        Assert.assertFalse(repositoryUser.getLocked());
    }

    @NotNull
    private User createUser() {
        @NotNull final User user = new User();
        user.setId(userId);
        user.setLogin(USER_LOGIN_PREFIX);
        user.setPasswordHash(HashUtil.salt(USER_PASSWORD_PREFIX, USER_PASSWORD_SECRET, USER_PASSWORD_ITERATION));
        user.setRole(Role.ADMIN);
        user.setEmail(USER_EMAIL_PREFIX);
        return user;
    }

    @NotNull
    private List<Task> createManyTasks() {
        @NotNull final List<Task> tasks = new Vector<>();
        for (int i = 0; i < REPOSITORY_SIZE; i++) {
            Task task = new Task();
            task.setName(TASK_NAME_PREFIX + i);
            task.setDescription(TASK_DESCRIPTION_PREFIX + i);
            task.setUserId(userId);
            task.setProjectId(projectId);
            tasks.add(task);
        }
        return tasks;
    }

    @NotNull
    private Project createOneProject() {
        @NotNull final Project project = new Project();
        project.setId(projectId);
        project.setUserId(userId);
        project.setName(PROJECT_NAME_PREFIX);
        project.setDescription(PROJECT_DESCRIPTION_PREFIX);
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

}
