package ru.t1consulting.nkolesnik.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.IProjectService;
import ru.t1consulting.nkolesnik.tm.api.service.IPropertyService;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.ModelNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.StatusNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.*;
import ru.t1consulting.nkolesnik.tm.marker.DataCategory;
import ru.t1consulting.nkolesnik.tm.model.Project;

import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import java.util.UUID;
import java.util.Vector;

@Category(DataCategory.class)
public class ProjectServiceTest {

    @NotNull
    private static final String PROJECT_NAME_PREFIX = "TEST_PROJECT_NAME";

    @NotNull
    private static final String PROJECT_DESCRIPTION_PREFIX = "TEST_PROJECT_DESCRIPTION";

    @Nullable
    private static final String NULL_PROJECT_NAME = null;

    @Nullable
    private static final String NULL_PROJECT_DESCRIPTION = null;

    @Nullable
    private static final Status NULL_STATUS = null;

    @Nullable
    private static final String NULL_PROJECT_ID = null;

    @Nullable
    private static final Integer NULL_PROJECT_INDEX = null;

    @Nullable
    private static final String NULL_USER_ID = null;

    @NotNull
    private static final String EMPTY_PROJECT_ID = "";

    @NotNull
    private static final String EMPTY_USER_ID = "";

    @Nullable
    private static final Project NULL_PROJECT = null;

    private static final long REPOSITORY_SIZE = 10L;

    private static final long EMPTY_REPOSITORY_SIZE = 0L;

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String projectId = UUID.randomUUID().toString();

    @NotNull
    private List<Project> projects;

    @NotNull
    private Project project;

    @BeforeClass
    public static void prepareTestEnvironment() {
        createDbBackup();
    }

    @AfterClass
    public static void restoreProdEnvironment() {
        restoreDbBackup();
    }

    @SneakyThrows
    private static void createDbBackup() {
        final IPropertyService propertyService = new PropertyService();
        final IConnectionService connectionService = new ConnectionService(propertyService);
        Connection connection = connectionService.getConnection();
        String sqlSaveData = "SELECT * INTO \"backup_projects\" FROM \"projects\";";
        String sqlClearTable = "DELETE FROM \"projects\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlSaveData);
            statement.executeUpdate(sqlClearTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @SneakyThrows
    private static void restoreDbBackup() {
        final IPropertyService propertyService = new PropertyService();
        final IConnectionService connectionService = new ConnectionService(propertyService);
        Connection connection = connectionService.getConnection();
        String sqlClearTable = "DELETE FROM \"projects\";";
        String sqlLoadData = "INSERT INTO \"projects\" (SELECT * FROM \"backup_projects\");";
        String sqlDropBackupTable = "DROP TABLE \"backup_projects\";";
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(sqlClearTable);
            statement.executeUpdate(sqlLoadData);
            statement.executeUpdate(sqlDropBackupTable);
            connection.commit();
        } catch (Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Before
    public void setup() {
        project = createOneProject();
        projects = createManyProjects();
    }

    @After
    public void cleanup() {
        projectService.clear();
    }

    @Test
    public void add() {
        projectService.add(project);
        Assert.assertEquals(1, projectService.getSize());
        projectService.add(projects);
        Assert.assertEquals(REPOSITORY_SIZE + 1, projectService.getSize());
    }

    @Test
    public void set() {
        projectService.set(projects);
        Assert.assertEquals(REPOSITORY_SIZE, projectService.getSize());
    }

    @Test
    public void existById() {
        projectService.add(project);
        Assert.assertFalse(projectService.existsById(NULL_PROJECT_ID));
        Assert.assertFalse(projectService.existsById(EMPTY_PROJECT_ID));
        Assert.assertTrue(projectService.existsById(projectId));
    }

    @Test
    public void findAll() {
        projectService.add(projects);
        Assert.assertEquals(projects.size(), projectService.findAll().size());
    }

    @Test
    public void findById() {
        projectService.add(project);
        projectService.add(projects);
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findById(NULL_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findById(EMPTY_PROJECT_ID));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.findById(UUID.randomUUID().toString()));
        @Nullable final Project repositoryProject = projectService.findById(projectId);
        Assert.assertNotNull(repositoryProject);
        Assert.assertEquals(project.getId(), repositoryProject.getId());
    }

    @Test
    public void findByIndex() {
        projectService.add(project);
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findByIndex(NULL_PROJECT_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findByIndex(10000));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findByIndex(-1));
        @Nullable final Project repositoryProject = projectService.findByIndex(0);
        Assert.assertNotNull(repositoryProject);
        Assert.assertEquals(project.getId(), repositoryProject.getId());
    }

    @Test
    public void remove() {
        projectService.add(project);
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.remove(NULL_PROJECT));
        @Nullable final Project repositoryProject = projectService.remove(project);
        Assert.assertNotNull(repositoryProject);
        Assert.assertEquals(project.getId(), repositoryProject.getId());
    }

    @Test
    public void removeById() {
        projectService.add(project);
        projectService.add(projects);
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(NULL_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(EMPTY_PROJECT_ID));
        Assert.assertThrows(
                ModelNotFoundException.class,
                () -> projectService.removeById(UUID.randomUUID().toString())
        );
        @Nullable final Project repositoryProject = projectService.removeById(projectId);
        Assert.assertNotNull(repositoryProject);
        Assert.assertEquals(project.getId(), repositoryProject.getId());
    }

    @Test
    public void removeByIndex() {
        projectService.add(project);
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(NULL_PROJECT_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(10000));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(-1));
        @Nullable final Project repositoryProject = projectService.removeByIndex(0);
        Assert.assertNotNull(repositoryProject);
        Assert.assertEquals(project.getId(), repositoryProject.getId());
    }

    @Test
    public void removeAll() {
        projectService.add(projects);
        Assert.assertEquals(REPOSITORY_SIZE, projectService.getSize());
        projectService.removeAll(projects);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, projectService.getSize());
    }

    @Test
    public void clear() {
        projectService.add(projects);
        Assert.assertEquals(REPOSITORY_SIZE, projectService.getSize());
        projectService.clear();
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, projectService.getSize());
    }

    @Test
    public void getSize() {
        projectService.add(projects);
        Assert.assertEquals(REPOSITORY_SIZE, projectService.getSize());
    }

    @Test
    public void addWithUserId() {
        projectService.add(userId, project);
        Assert.assertEquals(1, projectService.getSize());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.add(NULL_USER_ID, project));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.add(EMPTY_USER_ID, project));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.add(userId, NULL_PROJECT));
    }

    @Test
    public void existByIdWithUserId() {
        projectService.add(userId, project);
        Assert.assertTrue(projectService.existsById(userId, projectId));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.existsById(NULL_USER_ID, projectId));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.existsById(EMPTY_USER_ID, projectId));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.existsById(userId, NULL_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.existsById(userId, EMPTY_PROJECT_ID));
    }

    @Test
    public void findAllWithUserId() {
        projectService.add(userId, project);
        Assert.assertEquals(1, projectService.findAll(userId).size());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(EMPTY_USER_ID));
    }

    @Test
    public void findByIdWithUserId() {
        projectService.add(userId, project);
        @Nullable final Project repositoryProject = projectService.findById(userId, projectId);
        Assert.assertNotNull(repositoryProject);
        Assert.assertEquals(project.getId(), repositoryProject.getId());
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findById(NULL_USER_ID, projectId));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findById(EMPTY_USER_ID, projectId));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findById(userId, NULL_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findById(userId, EMPTY_PROJECT_ID));
    }

    @Test
    public void findByIndexWithUserId() {
        projectService.add(userId, project);
        Assert.assertThrows(
                IndexIncorrectException.class,
                () -> projectService.findByIndex(userId, NULL_PROJECT_INDEX)
        );
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findByIndex(userId, 10000));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findByIndex(userId, -1));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findByIndex(NULL_USER_ID, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findByIndex(EMPTY_USER_ID, 0));
        @Nullable final Project repositoryProject = projectService.findByIndex(userId, 0);
        Assert.assertNotNull(repositoryProject);
        Assert.assertEquals(project.getId(), repositoryProject.getId());
    }

    @Test
    public void removeWithUserId() {
        projectService.add(userId, project);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.remove(NULL_USER_ID, project));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.remove(EMPTY_USER_ID, project));
        Assert.assertThrows(ModelNotFoundException.class, () -> projectService.remove(userId, NULL_PROJECT));
        @Nullable final Project repositoryProject = projectService.remove(userId, project);
        Assert.assertNotNull(repositoryProject);
        Assert.assertEquals(project.getId(), repositoryProject.getId());
    }

    @Test
    public void removeByIdWithUserId() {
        projectService.add(userId, project);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeById(NULL_USER_ID, projectId));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeById(EMPTY_USER_ID, projectId));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(userId, NULL_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(userId, EMPTY_PROJECT_ID));
        Assert.assertThrows(
                ModelNotFoundException.class,
                () -> projectService.removeById(userId, UUID.randomUUID().toString())
        );
        @Nullable final Project repositoryProject = projectService.removeById(userId, projectId);
        Assert.assertNotNull(repositoryProject);
        Assert.assertEquals(project.getId(), repositoryProject.getId());
    }

    @Test
    public void removeByIndexWithUserId() {
        projectService.add(project);
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeByIndex(NULL_USER_ID, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeByIndex(EMPTY_USER_ID, 0));
        Assert.assertThrows(
                IndexIncorrectException.class,
                () -> projectService.removeByIndex(userId, NULL_PROJECT_INDEX)
        );
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(10000));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(-1));
        @Nullable final Project repositoryProject = projectService.removeByIndex(userId, 0);
        Assert.assertNotNull(repositoryProject);
        Assert.assertEquals(project.getId(), repositoryProject.getId());
    }

    @Test
    public void clearWithUserId() {
        projectService.add(userId, project);
        Assert.assertEquals(1, projectService.getSize(userId));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(EMPTY_USER_ID));
        projectService.clear(userId);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, projectService.getSize());
    }

    @Test
    public void getSizeWithUserId() {
        projectService.add(userId, project);
        Assert.assertEquals(1, projectService.getSize(userId));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.getSize(NULL_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.getSize(EMPTY_USER_ID));
        projectService.clear(userId);
        Assert.assertEquals(EMPTY_REPOSITORY_SIZE, projectService.getSize());
    }

    @Test
    public void create() {
        projectService.add(userId, project);
        Assert.assertEquals(1, projectService.getSize(userId));
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> projectService.create(NULL_USER_ID, PROJECT_NAME_PREFIX)
        );
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(userId, NULL_PROJECT_NAME));
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> projectService.create(userId, PROJECT_NAME_PREFIX, NULL_PROJECT_DESCRIPTION)
        );
    }

    @Test
    public void updateById() {
        projectService.add(userId, project);
        Assert.assertEquals(1, projectService.getSize(userId));
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> projectService.updateById(
                        NULL_USER_ID, projectId,
                        PROJECT_NAME_PREFIX,
                        PROJECT_DESCRIPTION_PREFIX
                )
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectService.updateById(
                        userId, NULL_PROJECT_ID,
                        PROJECT_NAME_PREFIX,
                        PROJECT_DESCRIPTION_PREFIX
                )
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> projectService.updateById(userId, projectId, NULL_PROJECT_NAME, PROJECT_DESCRIPTION_PREFIX)
        );
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> projectService.updateById(userId, projectId, PROJECT_NAME_PREFIX, NULL_PROJECT_DESCRIPTION)
        );
    }

    @Test
    public void updateByIndex() {
        projectService.add(userId, project);
        Assert.assertEquals(1, projectService.getSize(userId));
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> projectService.updateByIndex(
                        NULL_USER_ID,
                        0,
                        PROJECT_NAME_PREFIX,
                        PROJECT_DESCRIPTION_PREFIX
                )
        );
        Assert.assertThrows(
                IndexIncorrectException.class,
                () -> projectService.updateByIndex(
                        userId,
                        NULL_PROJECT_INDEX,
                        PROJECT_NAME_PREFIX,
                        PROJECT_DESCRIPTION_PREFIX
                )
        );
        Assert.assertThrows(
                NameEmptyException.class,
                () -> projectService.updateByIndex(userId, 0, NULL_PROJECT_NAME, PROJECT_DESCRIPTION_PREFIX));
        Assert.assertThrows(
                DescriptionEmptyException.class,
                () -> projectService.updateByIndex(userId, 0, PROJECT_NAME_PREFIX, NULL_PROJECT_DESCRIPTION));
    }

    @Test
    public void changeStatusById() {
        projectService.add(userId, project);
        Assert.assertEquals(1, projectService.getSize(userId));
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> projectService.changeProjectStatusById(NULL_USER_ID, projectId, Status.NOT_STARTED)
        );
        Assert.assertThrows(
                ProjectIdEmptyException.class,
                () -> projectService.changeProjectStatusById(userId, NULL_PROJECT_ID, Status.NOT_STARTED)
        );
        Assert.assertThrows(
                StatusNotFoundException.class,
                () -> projectService.changeProjectStatusById(userId, projectId, NULL_STATUS)
        );
    }

    @Test
    public void changeStatusByIndex() {
        projectService.add(userId, project);
        Assert.assertEquals(1, projectService.getSize(userId));
        Assert.assertThrows(
                UserNotFoundException.class,
                () -> projectService.changeProjectStatusByIndex(NULL_USER_ID, 0, Status.IN_PROGRESS)
        );
        Assert.assertThrows(
                IndexIncorrectException.class,
                () -> projectService.changeProjectStatusByIndex(userId, NULL_PROJECT_INDEX, Status.IN_PROGRESS)
        );
        Assert.assertThrows(
                StatusNotFoundException.class,
                () -> projectService.changeProjectStatusByIndex(userId, 0, NULL_STATUS)
        );
    }

    @NotNull
    private Project createOneProject() {
        @NotNull final Project project = new Project();
        project.setId(projectId);
        project.setUserId(userId);
        project.setName(PROJECT_NAME_PREFIX);
        project.setDescription(PROJECT_DESCRIPTION_PREFIX);
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @NotNull
    private List<Project> createManyProjects() {
        @NotNull final List<Project> projects = new Vector<>();
        for (int i = 0; i < REPOSITORY_SIZE; i++) {
            Project project = new Project();
            project.setName(PROJECT_NAME_PREFIX + i);
            project.setDescription(PROJECT_DESCRIPTION_PREFIX + i);
            project.setUserId(userId);
            projects.add(project);
        }
        return projects;
    }

}
