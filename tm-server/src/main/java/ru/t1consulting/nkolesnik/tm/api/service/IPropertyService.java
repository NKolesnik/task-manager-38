package ru.t1consulting.nkolesnik.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getSessionKey();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getSessionTimeout();

    @NotNull
    String getDatabaseConnectionString();

    @NotNull
    String getDatabaseUsername();

    @NotNull
    String getDatabasePassword();

}
