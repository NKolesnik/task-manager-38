package ru.t1consulting.nkolesnik.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.repository.ITaskRepository;
import ru.t1consulting.nkolesnik.tm.api.service.IConnectionService;
import ru.t1consulting.nkolesnik.tm.api.service.ITaskService;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.ModelNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.StatusNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.DescriptionEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.IndexIncorrectException;
import ru.t1consulting.nkolesnik.tm.exception.field.NameEmptyException;
import ru.t1consulting.nkolesnik.tm.exception.field.TaskIdEmptyException;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    protected @NotNull ITaskRepository getRepository(@NotNull Connection connection) {
        return new TaskRepository(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        @NotNull final Connection connection = getConnection();
        @Nullable Task result;
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            result = repository.create(userId, name);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final Connection connection = getConnection();
        @Nullable Task result;
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            result = repository.create(userId, name, description);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final Connection connection = getConnection();
        @Nullable Task result;
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            result = repository.create(userId, name);
            result.setDateBegin(dateBegin);
            result.setDateEnd(dateEnd);
            repository.update(result);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(id).orElseThrow(TaskIdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final Connection connection = getConnection();
        @Nullable Task result;
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            result = repository.findById(userId, id);
            if (result == null) throw new ModelNotFoundException();
            result.setName(name);
            result.setDescription(description);
            repository.update(result);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(index).filter(idx -> idx > -1).orElseThrow(IndexIncorrectException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        Optional.ofNullable(description).orElseThrow(DescriptionEmptyException::new);
        @NotNull final Connection connection = getConnection();
        @Nullable Task result;
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            result = repository.findByIndex(userId, index);
            if (result == null) throw new ModelNotFoundException();
            result.setName(name);
            result.setDescription(description);
            repository.update(result);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(index).filter(idx -> idx > -1).orElseThrow(IndexIncorrectException::new);
        Optional.ofNullable(status).orElseThrow(StatusNotFoundException::new);
        @NotNull final Connection connection = getConnection();
        @Nullable Task result;
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            result = repository.findByIndex(userId, index);
            if (result == null) throw new ModelNotFoundException();
            result.setStatus(status);
            repository.update(result);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeTaskStatusById(@Nullable final String userId, final String id, @Nullable final Status status) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(id).orElseThrow(TaskIdEmptyException::new);
        Optional.ofNullable(status).orElseThrow(StatusNotFoundException::new);
        @NotNull final Connection connection = getConnection();
        @Nullable Task result;
        try {
            @NotNull final ITaskRepository repository = getRepository(connection);
            result = repository.findById(userId, id);
            if (result == null) throw new ModelNotFoundException();
            result.setStatus(status);
            repository.update(result);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @Nullable List<Task> result;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final ITaskRepository repository = getRepository(connection);
            result = repository.findAllByProjectId(userId, projectId);
            if (result.isEmpty()) return Collections.emptyList();
            return result;
        }
    }


}
