package ru.t1consulting.nkolesnik.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.api.repository.IUserOwnedRepository;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) return null;
        if (model == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findById(userId, id) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) {
        @NotNull final List<M> models = new ArrayList<>();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final String sqlCommand = "SELECT * FROM " + getTableName() + " WHERE \"user_id\" = ?;";
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, userId);
            @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                models.add(fetch(resultSet));
            }
        }
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        @NotNull final List<M> models = new ArrayList<>();
        if (comparator == null) return findAll(userId);
        @NotNull final String comparatorName = comparator.toString();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final String sqlCommand = "SELECT * FROM " +
                getTableName() +
                " WHERE \"user_id\" = ? ORDER BY " +
                formatColumnName(comparatorName) +
                " ASC;";
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, userId);
            @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                models.add(fetch(resultSet));
            }
        }
        return models;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (sort == null) return findAll(userId);
        final Comparator<M> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) return null;
        if (index == null) return null;
        if (index < 0 || index > getSize() - 1) return null;
        @NotNull final String sqlCommand = "SELECT * FROM " + getTableName() + " WHERE \"user_id\" = ? LIMIT 1 OFFSET ?;";
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, userId);
            preparedStatement.setInt(2, index);
            @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final String sqlCommand = "SELECT * FROM " + getTableName() + " WHERE \"user_id\" = ? AND \"id\" = ?;";
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, userId);
            preparedStatement.setString(2, id);
            @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) return null;
        if (model == null) return null;
        @NotNull final String id = model.getId();
        return removeById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @Nullable final M model = findById(userId, id);
        if (model == null) return null;
        @NotNull final String sqlCommand = "DELETE FROM " + getTableName() + " WHERE \"user_id\" = ? AND \"id\" = ?;";
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, userId);
            preparedStatement.setString(2, id);
            preparedStatement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) return null;
        if (index == null) return null;
        if (index < 0 || index > getSize() - 1) return null;
        @Nullable final M model = findByIndex(userId, index);
        if (model == null) return null;
        return remove(userId, model);
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        @NotNull final String sqlCommand = "DELETE FROM " + getTableName() + " WHERE \"user_id\" = ?;";
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, userId);
            preparedStatement.executeUpdate();
        }
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return 0;
        @NotNull final String sqlCommand = "SELECT COUNT(1) as \"CNT\"" +
                " FROM " + getTableName() + " WHERE \"user_id\" = ?;";
        try (@NotNull final PreparedStatement preparedStatement = connection.prepareStatement(sqlCommand)) {
            preparedStatement.setString(1, userId);
            @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getLong("CNT");
            }
            return 0;
        }
    }

    @Override
    public @Nullable M update(@NotNull M model) {
        return null;
    }
}
